#include <iostream>
#include <string>
#include <chrono>
#include <thread>  
// this_thread::sleep_for(chrono::milliseconds(5000));
// https://www.cplusplus.com/reference/thread/this_thread/sleep_for/ 

using namespace std;

char choice1 = 'z';
char choice2 = 'z';
char choice3 = 'z';

char correct1 = 'a';
char correct11 = 's';
char correct2 = 'd';
char correct21 = 'f';
char correct3 = 'g';
char correct31 = 'h';

char replay;

void q1();
void q2();
void q3();
void win();
void again();
void quit();

int i;

string welcome = 
"#############################\n"
"Hello. Are you an as/df/gh master?\n"
"#############################";
string winmessage = "You win!\n";
string losemessage = "You lose!\n";

void aSwap() {
	swap(correct1, correct11);
	swap(correct2, correct21);
	swap(correct3, correct31);
}

void bSwap() {
	while(replay != 'n') {
	this_thread::sleep_for(100ms);
	swap(correct1, correct11);
	swap(correct2, correct21);
	swap(correct3, correct31);
	}
}

void q1() {
    if(choice1 == correct1) {
        	choice1 = 'z';
        	replay = 'z';
        	q2();
        	} else {
        		cout << "a or s?" << endl;
        		do {
        			aSwap(); 
        			cin >> choice1;
        		} while (choice1 == 'z');
        		q1();
        		}
}

void q2() {
    if(choice2 == correct2) {
        	choice2 = 'z';
        	q3();
        	} else {
        		cout << "d or f?" << endl;
        		do {
        			aSwap(); 
        			cin >> choice2;
        		} while (choice2 == 'z');
        		q2();
        		}
}

void q3() {
    if(choice3 == correct3) {
        	choice3 = 'z';
        	win();
        	} else {
        		cout << "g or h?" << endl;
        		do {
        			aSwap(); 
        			cin >> choice3;
        		} while (choice3 == 'z'); 
        		q3();
        		}
}

void win() { 
	cout << winmessage;
	if(replay == 'y') {
		q1();
		} else if(replay == 'n') {
			quit();
			} else {
				cout << ">Replay?\ny or n" << endl;
				cin >> replay;
				again();
				}
}

void again() {
	if(replay == 'y') {
		q1();
		} else if(replay == 'n') {
			quit();
			} else {
				cout << ">Replay?\ny or n" << endl;
				cin >> replay;
				again();
				}
}

void quit() {
	cout << "Bye!";
}

int main() {
    cout << welcome << endl;
    q1();
    
    return 0;
}