#include <iostream>
#include <string>

using namespace std;

char choice1 = 's';
char choice2 = 'f';
char choice3 = 'h';

char correct1 = 'a';
char correct2 = 'd';
char correct3 = 'g';

void q1();
void q2();
void q3();
void win();
void again();
void quit();

string winmessage = "You win!\n";
string losemessage = "You lose!\n";

char replay;

void q1() {
    if(choice1 == correct1) {
        	choice1 = 'z';
        	q2();
        	} else {
        		cout << "a or s?" << endl;
        		cin >> choice1;
        		q1();
        		}
}

void q2() {
    if(choice2 == correct2) {
        	choice2 = 'z';
        	q3();
        	} else {
        		cout << "d or f?" << endl;
        		cin >> choice2;
        		q2();
        		}
}

void q3() {
    if(choice3 == correct3) {
        	choice3 = 'z';
        	replay = 'z';
        	win();
        	again();
        	} else {
        		cout << "g or h?" << endl;
        		cin >> choice3;
        		q3();
        		}
}

void win() { 
	cout << winmessage;
}

void again() {
	if(replay == 'y') {
		q1();
		} else if(replay == 'n') {
			quit();
			} else {
				cout << "Replay?\ny or n" << endl;
				cin >> replay;
				again();
				}
}

void quit() {
	cout << "Bye!" << endl;
}

int main() {
    cout << "Hello. Are you an as/df/gh master?\n" << endl;
    
    q1();
    
    return 0;
}