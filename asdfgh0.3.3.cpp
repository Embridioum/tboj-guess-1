#include <iostream>
#include <string>

using namespace std;

string weekday[7] = {
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"Sunday", 
	};

int i;
int j;
int initiate;

char choice1 = 'z';
char choice2 = 'z';
char choice3 = 'z';

char correct1 = 'a';
char correct11 = 's';
char correct2 = 'd';
char correct21 = 'f';
char correct3 = 'g';
char correct31 = 'h';

char replay;

void q1();
void q2();
void q3();
void win();
void again();
void quit();

string welcome = 
"#######################\n"
"Are you an as/df/gh master?\n"
"Start the game by entering a\n"
"number between 1 and 10.";
string winmessage = "You win!\n";

void aSwap() {
	for (i = j; i > 0; i--) {
	swap(correct1, correct11);
	swap(correct2, correct21);
	swap(correct3, correct31);
	}
}

void q1() {
    if(choice1 == correct1) {
        	choice1 = 'z';
        	replay = 'z';
        	q2();
        	} else {
        		cout << "a or s?" << endl;
        		do {
        			j = j + i + 1;
        			aSwap(); 
        			cin >> choice1;
        		} while (choice1 == 'z');
        		q1();
        		}
}

void q2() {
    if(choice2 == correct2) {
        	choice2 = 'z';
        	initiate++;
        	q3();
        	} else {
        		cout << "d or f?" << endl;
        		do {
        			j = j + i + 1;
        			aSwap(); 
        			cin >> choice2;
        		} while (choice2 == 'z');
        		q2();
        		}
}

void q3() {
    if(choice3 == correct3) {
        	choice3 = 'z';
        	initiate++;
        	win();
        	} else {
        		cout << "g or h?" << endl;
        		do {
        			j = j + i + 1;
        			aSwap(); 
        			cin >> choice3;
        		} while (choice3 == 'z'); 
        		q3();
        		}
}

void win() { 
	cout << winmessage;
	if(replay == 'y') {
		j = j + i + 2;
		q1();
		} else if(replay == 'n') {
			quit();
			} else {
				cout << ">Replay?\ny or n" << endl;
				cin >> replay;
				again();
				}
}

void again() {
	if(replay == 'y') {
		q1();
		} else if(replay == 'n') {
			quit();
			} else {
				cout << ">Replay?\ny or n" << endl;
				cin >> replay;
				again();
				}
}

void quit() {
	cout << "Bye!";
}

int main() {
    cout << welcome << endl;
    cin >> i; 
    j = i;
    q1();
    
    return 0;
}