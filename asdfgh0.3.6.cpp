#include <iostream>
#include <string>
#include <random>

// https://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution

using namespace std;

int i;

void initiate();

char choice1 = 'z';
char choice2 = 'z';
char choice3 = 'z';

char correct1 = 'a';
char correct11 = 's';
char correct2 = 'd';
char correct21 = 'f';
char correct3 = 'g';
char correct31 = 'h';

char replay;

void q1();
void q2();
void q3();
void win();
void again();
void quit();

string welcome = 
"#######################\n"
"Are you an as/df/gh master?\n";

string winmessage = "You win!\n";

void initiate() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(1, 9);
    i = distrib(gen);
}

void aSwap() {
	initiate();
	for (i; i > 0; i--) {
	swap(correct1, correct11);
	swap(correct2, correct21);
	swap(correct3, correct31);
	}
}

void q1() {
    if(choice1 == correct1) {
        	choice1 = 'z';
        	replay = 'z';
        	q2();
        	} else {
        		cout << "1. a or s?" << endl;
        		aSwap(); 
        		cin >> choice1;
        		q1();
        		}
}

void q2() {
    if(choice2 == correct2) {
        	choice2 = 'z';
        	q3();
        	} else {
        		cout << "2. d or f?" << endl;
        		aSwap(); 
        		cin >> choice2;
        		q2();
        		}
}

void q3() {
    if(choice3 == correct3) {
        	cout << winmessage;
        	choice3 = 'z';
        	win();
        	} else {
        		cout << "3. g or h?" << endl;
        		aSwap(); 
        		cin >> choice3;
        		q3();
        		}
}

void win() {
	if(replay == 'y') {
		q1();
		} else if(replay == 'n') {
			quit();
			} else {
				cout << ">Replay?\ny or n" << endl;
				cin >> replay;
				win();
				}
}

void quit() {
	cout << "Bye!";
}

int main() {
    cout << welcome << endl;
//   j = i;
    q1();
 
/*
	for(k = 100; k > 0; k--) {
	   cout << i << endl;
	   initiate();
	}
*/
	
    return 0;
}