#include <iostream>
#include <string>
#include <random>

// https://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution

using namespace std;

int i;
int j;
int k = -1;

void initiate();

char choice1 = 'z';
char choice2 = 'z';
char choice3 = 'z';

char correct1 = 'a';
char correct11 = 's';
char correct2 = 'd';
char correct21 = 'f';
char correct3 = 'g';
char correct31 = 'h';

char replay;

void q1();
void q2();
void q3();
void win();
void lose();
void again();
void quit();

string welcome = 
"#######################\n"
"Are you an as/df/gh master?\n"
"A total of three incorrect\n"
"answers and you will lose.\n"
"Do you feel lucky?\n"
"#######################\n";

string winmessage = "You win!\n";
string losemessage = "You lose!";

void initiate() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(1, 9);
    i = distrib(gen);
}

void aSwap() {
	initiate();
	for (j = i; i > 0; i--) {
	swap(correct1, correct11);
	swap(correct2, correct21);
	swap(correct3, correct31);
	}
}

void q1() {
    if(k < 3) {
    if(choice1 == correct1) {
        	choice1 = 'z';
        	replay = 'z';
        	q2();
        	} else {
        		k++;
        		cout << "1. a or s?" << endl;
        		aSwap(); 
        		cin >> choice1;
        		q1();
        		}
    } else {
    	lose();
    }
}

void q2() {
    if(k < 3) {
    if(choice2 == correct2) {
        	choice2 = 'z';
        	k--;
        	q3();
        	} else {
        		k++;
        		cout << "2. d or f?" << endl;
        		aSwap(); 
        		cin >> choice2;
        		q2();
        		}
    } else {
    	lose();
    }
}

void q3() {
    if(k < 3) {
    if(choice3 == correct3) {
        	cout << winmessage;
        	choice3 = 'z';
        	k--;
        	win();
        	} else {
        		k++;
        		cout << "3. g or h?" << endl;
        		aSwap(); 
        		cin >> choice3;
        		q3();
        		}
        } else {
    		lose();
    } 
}

void win() {
	if(replay == 'y') {
		k = -1;
		choice1 = 'z';
		choice2 = 'z';
		choice3 = 'z'; 
		q1();
		} else if(replay == 'n') {
			quit();
			} else {
				cout << ">Replay?\ny or n" << endl;
				cin >> replay;
				win();
				}
}

void lose() {
	cout << losemessage << endl;
	win();
}

void quit() {
	cout << "Bye!";
}

int main() {
    cout << welcome << endl;
    q1();
	
    return 0;
}(k = 100; k > 0; k--) {
	   cout << i << endl;
	   initiate();
	}
*/
	
    return 0;
}